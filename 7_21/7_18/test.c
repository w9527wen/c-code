#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>


//int main()
//{
//	char ch = 'a';
//	char* pc = &ch;
//	*pc = 's';
//	printf("%c", ch);
//	return 0;
//}
//int main()
//{
//	const char* p1 = "hello world";
//	const char* p2 = "hello world";
//
//	printf("%p\n", p1);
//	printf("%p", p2);
//
//}
//

//
void print1(int arr[5][2], int r, int c)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < r; i++)
	{
		for (j = 0; j < c; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
}

void print2(int(*p)[2], int r, int c)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < r; i++)
	{
		for (j = 0; j < c; j++)
		{
			printf("%d ", p[i][j]);
		}
		printf("\n");
	}
}

int main()
{
	int arr[5][2] = { 1,2,3,4,5,6,7,8,9,10 };
	print1(arr, 5, 2);

	print2(arr, 5, 2);
	return 0;
}