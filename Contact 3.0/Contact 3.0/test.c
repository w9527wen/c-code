#define _CRT_SECURE_NO_WARNINGS 1

#include"contact.h"

void menu()
{
	printf("*************************************\n");
	printf("******   1.add       2.del     ******\n");
	printf("******   3.search    4.modify  ******\n");
	printf("******   5.sort      6.print   ******\n");
	printf("******   0.exit               ******\n");
	printf("*************************************\n");
}
void test()
{
	int input = 0;

	contact con;

	Init_Contact(&con);

	do
	{
		menu();
		printf("请输入:>");
		scanf("%d", &input);
		switch (input)
		{
		case EXIT:
			save_contact(&con);
			destroy_contact(&con);
			printf("退出通讯录\n");
			break;
		case ADD:
			Addcontact(&con);
			break;
		case DEL:
			Delcontact(&con);
			break;
		case SEARCH:
			Searchcontact(&con);
			break;
		case MODIFY:
			Modifycontact(&con);
			break;
		case SORT:
			Sortcontact(&con);
			break;
		case PRINT:
			Printcontact(&con);
			break;
		default:
			printf("输入错误，重新输入\n");
		}

	} while (input);

}


int main()
{
	test();
	return 0;
}