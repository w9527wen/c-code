#define _CRT_SECURE_NO_WARNINGS 1


#include"contact.h"


void cheak_capacity(contact* pc)
{
	if (pc->sz == pc->capacity)
	{
		pc->data = (peoinfo*)realloc(pc->data, (pc->capacity + 2) * sizeof(peoinfo));
		if (pc->data == NULL)
		{
			perror("check_capacity:realloc:");
			return;
		}
		pc->capacity += 2;
	}

	printf("增容成功！\n");
}
 
//从文件读取数据

void load_contact(contact* pc)
{
	FILE* pf = fopen("contact.tt", "rb");
	peoinfo tmp = { 0 };

	if (pf == NULL)
	{
		perror("save_fopen");
	}
	else
	{
		while (fread(&tmp, sizeof(peoinfo), 1, pf))   // fread 函数的返回值为它读取的数据个数，读完数据后，返回值为0，循环结束
		{
			cheak_capacity(pc);
			pc->data[pc->sz] = tmp;
			pc->sz++;
		}
		
		fclose(pf);
		pf = NULL;
	}
}
//初始化   文件版本
void Init_Contact(contact* pc)
{
	pc->sz = 0;
	pc->capacity = capMAX;
	pc->data = (peoinfo*)malloc(pc->capacity * sizeof(peoinfo));
	if (pc->data == NULL)
	{
		perror("Init_Contact:malloc:");
	}
	memset(pc->data, 0, pc->capacity * sizeof(peoinfo));
	load_contact(pc);

}





//增加
void Addcontact(contact* pc)
{
	cheak_capacity(pc);

	printf("请输入姓名:>");
	scanf("%s", pc->data[pc->sz].name);

	printf("请输入号码:>");
	scanf("%s", pc->data[pc->sz].tele);

	pc->sz++;
	printf("添加成功\n");
}



// 打印
void Printcontact(contact* pc)
{
	printf("%-7s,%-12s\n", "姓名", "号码");
	int j = 0;
	for (j = 0; j < pc->sz; j++)
	{
		printf("%-7s %-12s\n", pc->data[j].name, pc->data[j].tele);
	}

}


//封装一个查找函数    找到了返回下标，没找到返回 -1
int FindByName(contact* pc, char arr[])
{
	int j = 0;
	for (j = 0; j < pc->sz; j++)
	{
		if (0 == strcmp(pc->data[j].name, arr))
		{
			return j;
		}
	}
	return -1;
}

//删除
void Delcontact(contact* pc)
{
	assert(pc);
	//查找
	char arr[NAME_MAX] = { 0 };

	printf("请输入姓名:>");

	scanf("%s", arr);

	int b = FindByName(pc, arr);
	//删除
	if (b == -1)
	{
		printf("未找到联系人\n");
		return;
	}
	int j = 0;
	for (j = b; j < pc->sz - 1; j++)
	{
		pc->data[j] = pc->data[j + 1];
	}
	pc->sz--;
	printf("删除成功。\n");
}


//查找
void Searchcontact(contact* pc)
{
	assert(pc);

	printf("请输入要查找联系人的姓名:>");

	char arr[NAME_MAX] = { 0 };

	scanf("%s", arr);

	int b = FindByName(pc, arr);

	if (b == -1)
	{
		printf("未找到联系人\n");
		return;
	}
	printf("%7s,%12s", "姓名", "号码");
	printf("%7s %12s\n", pc->data[b].name, pc->data[b].tele);

}


//修改

void Modifycontact(contact* pc)
{
	printf("请输入需要修改联系人的姓名:>");

	char arr[NAME_MAX] = { 0 };

	scanf("%s", arr);

	int b = FindByName(pc, arr);

	if (b == -1)
	{
		printf("未找到联系人\n");
		return;
	}
	printf("请输入修改后的姓名:>");
	scanf("%s", pc->data[b].name);
	printf("请输入修改后的联系人电话号码:>");
	scanf("%s", pc->data[b].tele);
	printf("修改完成\n");

}


//排序   qsort排序
void cmp_by_name(const void* p1, const void* p2)
{
	return strcmp(((contact*)p1)->data->name, ((contact*)p2)->data->name);
}

void Sortcontact(contact* pc)
{
	qsort(pc->data, pc->sz, sizeof(pc->data[0]), cmp_by_name);
	printf("排序成功.");
}



//销毁通讯录
void destroy_contact(contact* pc)
{
	free(pc->data);
	pc->data = NULL;
	pc->sz = 0;
	pc->capacity = 0;
	printf("销毁成功\n");
}

//将输入的数据存入文件中

void save_contact(contact* pc)
{
	FILE* pf = fopen("contact.tt","wb");

	int i = 0;

	if (pf == NULL)
	{
		perror("save_fopen");
	}
	else
	{
		for (i = 0; i < pc->sz; i++)
		{
			fwrite(pc->data + i, sizeof(peoinfo), 1, pf);
		}
		fclose(pf);
		pf = NULL;
		printf("保存文件成功");
	}

}
