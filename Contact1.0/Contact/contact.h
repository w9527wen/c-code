#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<assert.h>
#include<stdlib.h>

#define MAX 1000

#define NAME_MAX 7
#define TELE_MAX 12

enum option
{
	EXIT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,
	SORT,
	PRINT,
};

typedef struct peoinfo
{
	char name[NAME_MAX];
	char tele[TELE_MAX];
}peoinfo;


typedef struct contact
{
	peoinfo data[MAX];

	int sz;
}contact;

 //  初始化通讯录
void Init_Contact(contact *pc);


//增加函数
void Addcontact(contact *pc);


//打印函数
void Printcontact(contact* pc);


//删除函数
void Delcontact(contact* pc);

//查找函数
void Searchcontact(contact* pc);

//修改函数
void Modifycontact(contact* pc);

//排序函数,,按照姓名排序
void Sortcontact(contact* pc);




