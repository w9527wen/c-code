#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//void Swap(int a, int b)
//{
//	int tmp = a;
//	a = b;
//	b = tmp;
//}                           形参是实参的一份临时拷贝，对于形参的修改不会影响实参。


//void Swap(int* pa, int* pb)
//{
//	int tmp = *pa;
//	*pa = *pb;
//	*pb = tmp;
//}
//
//int main()
//{
//	int a = 20;
//	int b = 30;
//	printf("交换前 %d %d\n", a, b);
//	Swap(&a, &b);
//
//	printf("交换后 %d %d", a, b);
//
//
//	return 0;
//}
//void print(int i)
//{
//	for (int a = 1; a <= i; a++)
//	{
//		printf("%d*%d=%d ", a, i, a * i);
//	}
//}
//
//
//int main()
//{
//	int input = 0;
//	scanf("%d", &input);
//	print(input);
//	return 0;
//}

// int is_prm(int a)
//{
//	 int b = 0;
//	 for ( b = 2; b <= a; b++)
//	 {
//		 if (a % b == 0)
//		 {
//			 break;
//		 }
//	 }
//	 if (a == b)
//	 {
//		 return a;
//	 }
//	 else
//		 return 0;
//
//}
//
//int main()
//{
//	int count = 0;
//	int i = 100;
//	for (i = 101; i <= 200; i++)
//	{
//         int c = is_prm(i);
//		 if (c != 0)
//		 {
//			 printf("%d ", c);
//			 count++;
//		 }
//	}
//	printf("%d", count);
//
//	return 0;
//}
//
//




//int main()
//{
//	int a = 101;
//	int count = 0;
//
//	for (a = 101; a <= 200; a++)
//	{
//		int i;
//		for (i = 2; i < a; i++)
//		{
//			if (a % i == 0)
//				break;
//		}
//		if (a == i)
//		{
//			printf("%d ", a);
//
//			count++;
//		}
//
//	}
//	printf("%d", count);
//	return 0;
//}

int is_year(int i)
{
	if ((i % 4 == 0 && i % 100 != 0) || (i % 400 == 0))
	{
		return 1;
	}
	else
		return 0;
}


int main()
{
	int year = 0;
	scanf("%d", &year);
	int c = is_year(year);
	if (c == 1)
		printf("是闰年");
	else
		printf("不是闰年");
	return 0;
}