#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

void print(int a)
{
	if (a < 10)
	{
		printf("%d ", a);
	}
	else
		print(a % 10);
}


int main()
{
	int input = 0;

	scanf("%d", &input);

	print(input);

	return 0;
}