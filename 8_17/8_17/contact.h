#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>

#define name_max 20
#define tele_max 12

enum
{
	exit,
	add,
	del,
	search,
	modify,
	sort,
	print
};


typedef struct PeoInFo
{
	char name[name_max];
	
	char tele[tele_max];
	
}PeoInFo;

struct con
{
	PeoInFo data[1000];
	int sz;
};