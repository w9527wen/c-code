#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

// int main()
//{
//	int a = 10;//a是整型变量，占用4个字节的内存空间
//	int* pa = &a;
//
//	//pa是一个指针变量，用来存放地址的
//
//	//pa
//
//
//	//本质上指针就是地址
//	//口语中说的指针，其实是指针变量，指针变量就是一个变量，指针变量是用来存放地址的一个变量
//
//	return 0;
//}

//X86 - 32位的环境
//X64 - 64位的环境

//int main()
//{
//	char* pc = NULL;
//	short* ps = NULL;
//	int* pi = NULL;
//	double* pd = NULL;
//
//	//ptr_t pt = NULL;
//
//	//sizeof 返回的值的类型是无符号整型  unsigned int
//	printf("%zu\n", sizeof(pc));
//	printf("%zu\n", sizeof(ps));
//	printf("%zu\n", sizeof(pi));
//	printf("%zu\n", sizeof(pd));
//
//	return 0;
//}