#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"

void menu()
{
	printf("****************************\n");
	printf("********* 1.play  **********\n");
	printf("********* 0.exit  **********\n");
	printf("****************************\n");

}

void game()
{
	char mine[ROWS][COLS] = { 0 };
	char show[ROWS][COLS] = { 0 };


	InitBoard(mine, ROWS, COLS, '0');
	InitBoard(show, ROWS, COLS, '*');


	DisPlay(show, ROW, COL);

	SetMine(mine,ROW,COL);

	DisPlay(mine, ROW, COL);


	FindMine(mine, show, ROW, COL);

}



int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请输入:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏");
			break;
		default:
			printf("输入错误，重新输入:>");
			break;
		}
	} while (input);

	return 0;
}