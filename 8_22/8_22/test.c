#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<assert.h>

char* my_strcat(char* s1, const char* s2, int num)
{
	assert(s1 && s2);

	char* ret = s1;
	while (num-- && *s2)  // 如果 *s2 判断是否为 ' \0 '，避免越界访问引起错误
	{
		while (*s1)
		{
			s1++;
		}
		*s1++ = *s2++;
	}
	*s1 = '\0';
	return ret;
}


int main()
{
	char arr1[28] = "hello ";

	char arr2[] = "worlddddd";

	char* ret = my_strcat(arr1, arr2, 5);

	printf("%s\n", ret);
	printf("%d", sizeof(arr1));
	printf("%d", sizeof("hello world"));
	return 0;
}
