#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//#pragma pack(4) // 设置默认对齐数 为  4
//struct S3
//{
//	double d;
//
//	char c;
//
//	int i;
//};
//
//#pragma pack()  // 取消设置的对齐数，还原为默认。
//
//struct S4
//{
//	char c1;
//	struct S3 s3;
//	double d;
//
//};
//
//int main()
//{
//	printf("%d", sizeof(struct S4));
//}



//struct A
//{
//	int a : 2;
//	int b : 3;
//	unsigned c : 3;
//};
//


struct A
{
	char a : 3;
	char b : 4;
	char c : 5;
	char d : 6;
};

int main()
{

	struct A s = { 0 };
	s.a = 10;
	s.b = 12;
	s.c = 3;
	s.d = 4;
}