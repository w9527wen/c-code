#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>



//int my_strncmp(const char* str1, const char* str2, int num)
//{
//	assert(str1 && str2);
//
//	if (!num)  //  num = 0 直接返回0
//	{
//		return 0;
//	}
//	while(--num && *str1 && *str1 == *str2)  // 前置--  ，判断str 是否为 '\0 ',比较str，str2
//	{
//		str1++;
//		str2++;
//	}
//	return *str1 - *str2;
//}
//
//int main()
//{
//
//	char arr1[] = "abcdef";
//	char arr2[] = "abcdeg";
//
//    int ret  = my_strncmp(arr1, arr2, 6);
//
//	printf("%d", ret);
//
//	return 0;
//}



//
//char* my_strcat(char* s1, const char* s2,int num)
//{
//	assert(*s1 && *s2);
//
//	char* ret = s1;
//
//	while (num--)
//	{
//		while (*s1)
//		{
//			s1++;
//		}
//
//		*s1++ = *s2++;
//	}
//	return ret;
//}
//
//
//int main()
//{
//	char arr1[20] = "hello ";
//
//	char arr2[] = "worlddddd";
//
//	char* ret = my_strcat(arr1, arr2,5);
//
//	printf("%s", ret);
//	return 0;
//}



//char* my_strcpy(char* dest, char* sru,int num)
//{
//	char* s1 = dest;
//
//	assert(dest && sru);
//	while (num-- && *sru)
//	{
//		*dest++ = *sru++;
//	}
//	return s1;
//}
//
//
//int main()
//{
//	char arr1[20] = " ";
//
//	char arr2[] = "hai      ";
//
//	my_strcpy(arr1, arr2,10);
//
//	printf("%s", arr1);
//
//	return 0;
//}
//


//#include<string.h>
//
//int main()
//{
//	char arr1[] = "hello worid /siwen.2022_8_7";
//
//	char arr2[40] = { 0 };
//
//	strcpy(arr2, arr1);
//
//	char sep[] = " /.";
//
//	printf("%s\n", strtok(arr2, sep));
//	printf("%s\n", strtok(NULL, sep));
//	printf("%s\n", strtok(NULL, sep));
//	printf("%s\n", strtok(NULL, sep));
//
//	return 0;
//}
//




//
//#include<string.h>
//
//int main()
//{
//	char arr1[] = "hello worid /siwen.2022_8_7";
//
//	char arr2[40] = { 0 };
//
//	strcpy(arr2, arr1);
//
//	char sep[] = " /.";
//
//	char* str = NULL;
//
//	for (str = strtok(arr2, sep); str != NULL; str = strtok(NULL, sep))
//	{
//		printf("%s\n", str);
//	}
//
//	return 0;
//}

//



void* my_memcpy(void* dest, const void* sur, int num)
{
	assert(dest && sur);
	void* ret = dest;
	while (num--)
	{
		*((char*)dest) = *((char*)sur);
		dest = (char*)dest + 1;
		sur = (char*)sur + 1;
	}
	return  ret;
}

int main()
{
	int arr1[] = { 1,2,3,4,5,6,7,8 };

	int arr2[10] = { 0 };

	my_memcpy(arr2, arr1, 20);

	int i = 0;
	int sz = sizeof(arr2) / sizeof(arr2[0]);

	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr2[i]);
	}
	return 0;
}
