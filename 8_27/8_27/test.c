#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//
//int main()
//{
//	FILE* pf = fopen("text.tet", "w");
//
//	if (pf == NULL)
//	{
//		perror("fopen:");
//	}
//	else
//	{
//		fputs("abcdef", pf);
//
//		fclose(pf);
//		pf = NULL;
//	}
//	return 0;
//}

//#include<stdio.h>
//
//int main()
//{
//	FILE* pf = fopen("text.tet", "r");
//	char arr[100] = { 0 };
//	if (pf == NULL)
//	{
//		perror("fopen:");
//	}
//	else
//	{
//		fgets(arr, 7,pf);
//		printf("%s", arr);
//		
//		fclose(pf);
//		pf = NULL;
//	}
//	return 0;
//}



//#include<stdio.h>
//struct S
//{
//	char name[10];
//	int age;
//	double score;
//};
//
//int main()
//{
//	struct S s = { 0 };
//
//	FILE* pf = fopen("text.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen:");
//	}
//	else
//	{
//		fscanf(pf, "%s %d %lf", s.name, &(s.age), &(s.score));    // 从文件中输入信息到程序 结构体s 中
//		fprintf(stdout, "%s %d %lf", s.name, s.age, s.score);    // 将结构体s 的信息 直接输出到 标准输出流stdout (屏幕）
//
//	}
//}


//#include<stdio.h>
//
//struct S
//{
//	char name[10];
//	int age;
//	double score;
//};
//
//int main()
//{
//	struct S s = { "张三",10,8.3};
//	FILE* pf = fopen("text.tet", "wb");
//
//	if (pf == NULL)
//	{
//		perror("fopen");
//	}
//	else
//	{
//		fwrite(&s, sizeof(struct S), 1, pf);
//		fclose(pf);
//		pf = NULL;
//	}
//}

//
//#include<stdio.h>
//
//struct S
//{
//	char name[10];
//	int age;
//	double score;
//};
//
//int main()
//{
//	struct S s = {0};
//	FILE* pf = fopen("text.tet", "rb");
//
//	if (pf == NULL)
//	{
//		perror("fopen");
//	}
//	else
//	{
//		fread(&s, sizeof(struct S), 1, pf);
//		fprintf(stdout, "%s,%d,%lf", s.name, s.age, s.score);
//		fclose(pf);
//		pf = NULL;
//	}
//}


//#include<stdio.h>
//
//int main()
//{
//	FILE* pf = fopen("test1.txt", "w");
//	fputc('a', pf);
//	fputc('b', pf);
//	fputc('c', pf);
//	fputc('d', pf);
//
//	fseek(pf, 1, SEEK_SET);   // 将文件指针定位到和文件初始位置偏移量为1 的位置
//	fputc('j', pf);
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//
//}

//
//int main()
//{
//	int a = 10000;
//	FILE* pf = fopen("test2.txt", "wb");
//	fwrite(&a, 4, 1, pf);
//	fclose(pf);
//	pf = NULL;
//}


#include<stdio.h>

int main()
{
	FILE* pf = fopen("test3.txt", "w");
	fputs("abcdef", pf); //  先将数据放在缓冲区。
	Sleep(20000);  // 睡眠20秒，我们可以打开文件，发现没有内容
	printf("刷新缓冲区\n");
	fflush(pf);  // 刷新缓冲区，将数据写入磁盘。
	Sleep(20000);
	fclose(pf); //文件关闭时，会自动刷新缓冲区，上面的睡眠20秒，就是为了区别fflush 和 flose
	pf = NULL;
}
