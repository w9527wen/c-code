#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
#include <assert.h>

//1. 计数器的版本
//2. 递归的版本
//3. 指针-指针
//
//int my_strlen(const char* str)
//{
//	int count = 0;//计数器
//	assert(str != NULL);
//
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}

//int main()
//{
//	char arr[] = "abc";
//	//char arr[] = { 'a', 'b', 'c' };
//
//	int len  = my_strlen(arr);
//	//
//
//	printf("%d\n", len);
//
//	return 0;
//}


//int main()
//{
//	if (my_strlen("abc") - my_strlen("abcdef") > 0)
//	{
//		printf(">\n");
//	}
//	else
//	{
//		printf("<=\n");
//	}
//	return 0;
//}

//
//int main()
//{
//	char* str = "xxxxxxxxxxxxxxxxxxx";
//
//	//char arr[5] = "####";
//	//arr = "hello";//err
//	//char* p = "hello";
//	//strcpy(arr, "hello");//string copy
//	//char arr2[] = { 'a', 'b', 'c' };
//	char* p = "hello world";
//
//	strcpy(str, p);//?
//
//	//printf("%s\n", arr);
//
//	return 0;
//}
//
//
//
//
//char* my_strcat(char* dest, const char*src)
//{
//	char* ret = dest;
//	assert(dest && src);
//	//1. 找目标字符串中的\0
//	while (*dest)
//	{
//		dest++;
//	}
//	//2. 追加源字符串,包含\0
//	while(*dest++ = *src++)
//	{
//		;
//	}
//
//	return ret;//返回的目标空间的起始地址
//}
//
//int main()
//{
//	char arr1[20] = "hello ";//world
//	char arr2[] = "world";
//	//my_strcat(arr1, arr2);//字符串追加（连接）
//	printf("%s\n", my_strcat(arr1, arr2));
//
//	return 0;
//}
//
//int main()
//{
//	char arr[20] = "abcd";
//	strcat(arr, arr);//?
//	printf("%s\n", arr);
//	return 0;
////}
//
//int main()
//{
//	/*char* p = "obc";
//	char* q = "abcdef";
//	if (p > q)
//	{
//		printf(">\n");
//	}
//	else
//	{
//		printf("<=\n");
//	}*/
//
//	//if ("obc" > "abcdef")
//	//{
//
//	//}
//
//