#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int  Factorial(int i)
{
	int f = 1;

	if (i > 1)
	{
		f = i * Factorial(i - 1);
	}
	return f;
}


int main()
{
	int f = 0;
	int i = 0;
	scanf("%d", &i);

	printf("%d", Factorial(i));
	return 0;
}
