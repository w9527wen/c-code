#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <windows.h>


//int main()
//{
//	unsigned int i;
//	for (i = 9; i >= 0; i--)
//	{
//		printf("%u\n", i);
//		Sleep(1000);//休眠1000毫秒
//	}
//
//	return 0;
//}

//int main()
//{
//	char a[1000];
//	int i;
//
//	for (i = 0; i < 1000; i++)
//	{
//		a[i] = -1 - i;
//	}
//	//
//	//arr[i] --> char   -128~127
//	//-1 -2 -3 -4 ... -1000
//	//-1 -2 ... -128 127 126 125 .. 3 2 1 0 -1 ...
//	//128+127 = 255
//	printf("%d", strlen(a));//255
//	//strlen 是求字符串的长度，关注的是字符串中'\0'（数字0）之前出现多少字符
//
//	return 0;
//}

//#include <stdio.h>
//unsigned char i = 0;
//unsigned char 类型的取值范围是0~255
//
//int main()
//{
//	for (i = 0; i <= 255; i++)
//	{
//		printf("hello world\n");
//	}
//	return 0;
//}
//
//#include <assert.h>
//#include <string.h>
//
//int my_strlen(const char* str)
//{
//	assert(str);
//	int count = 0;
//	while (*str)
//	{
//		str++;
//		count++;
//	}
//	return count;
//}
//
//int main()
//{
//	//int len = strlen("abcdef");
//	//printf("%d\n", len);
//	//size_t -> unsigned int
//	//
//	if ((int)strlen("abc") - (int)strlen("abcdef")>0)
//		printf(">\n");
//	else
//		printf("<\n");
//
//	return 0;
//}


//int main()
//{
//	int count = 0;
//	for (int i = 1; i < 100; i++)
//	{
//		if (i % 10 == 9 )
//			count++;
//		if(i / 10 == 9)
//			count++;
//	}
//	printf("%d", count);
//}




//int main()
//{
//	int i = 0;
//	float sum = 0;
//	int t = 1;
//	for (i = 1; i <= 100; i++)
//	{
//		sum = sum + (t)*1.0 / i;
//		t = -t;
//	}
//	printf("%.3f", sum);
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	for (i = 0; i <= 10; ++i)
//		printf("%d\n", i);
//	return 0;
//}


//int Get_Max(int arr[])
//{
//	int q = arr[0];
//	for (int i = 0; i <= 10; i++)
//	{
//		if (arr[i] > arr[0])
//		{
//			arr[0] = arr[i];
//		}
//	}
//	return arr[0];
// }
//
//int main()
//{
//	int arr[] = { 2,3,4,6,7,8,9,34,42,423 };
//	int max = Get_Max(arr);
//
//	printf("%d", max);
//	return 0;
//}


int main()
{
	int i = 1;
	int a = 1;
	for (i = 1; i <= 9; i++)
	{
		for (a = 1; a <= i; a++)
			printf("%d*%d=%d ", a, i, i * a);
		printf("\n");
	}
	return 0;
}