#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>


//	char* arr[5] = {0};
//	char* (*pc)[5] = &arr;
//
//	//char ch = 'w';
//	//char* p1 = &ch;
//	//char** p2 = &p1;
//
//	//int arr[10] = { 0 };
//	////int* p = &arr;//有警告的
//	//int (*p2)[10] = &arr;
//	//p2的类型是int(*)[10];
//	//int(*)[10] p3;//err
//	
//	//整型指针是用来存放整型的地址
//	//字符指针是用来存放字符的地址
//	//数组指针是用来存放数组的地址
//	//
//	//printf("%p\n", arr);
//	//printf("%p\n", arr+1);
//
//	//printf("%p\n", &arr[0]);
//	//printf("%p\n", &arr[0]+1);
//
//	//printf("%p\n", &arr);
//	//printf("%p\n", &arr+1);
//
//
//	//int sz = sizeof(arr);
//	//printf("%d\n", sz);
//
//	return 0;
//}
//数组名通常表示的都是数组首元素的地址
//但是有2个例外：
//1. sizeof(数组名),这里的数组名表示整个数组，计算的是整个数组的大小
//2. &数组名，这里的数组名表示的依然是整个数组，所以&数组名取出的是整个数组的地址
//


//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//
//	//int*p = arr;
//	//int i = 0;
//	//for (i = 0; i < 10; i++)
//	//{
//	//	printf("%d ", *(p + i));
//	//}
//
//	//int (*p)[10] = &arr;
//	//
//	//int i = 0;
//	//int sz = sizeof(arr) / sizeof(arr[0]);
//	//for (i = 0; i < sz; i++)
//	//{
//	//	printf("%d ", *(*p+i));//p是指向数组的，*p其实就相当于数组名,数组名又是数组首元素的地址
//	//	//所以*p本质上是数组首元素的地址
//	//}
//
//	return 0;
//}