#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int check_sys()
{
	int a = 1;
	char* p = (char*)&a;
	return *p;
}


int main()
{

	int ret = check_sys();

	if (ret == 1)
	{
		printf("小端字节序\n");
     }
	else
	{
		printf("大端字节序\n");
	}

	return 0;
}

//小端字节序：低位字节序的内容放在低地址处。高位字节序的内容放在高地址处。
//大端字节序: 低位字节序的内容放在高地址处，高位字节序的内容放在低地址处