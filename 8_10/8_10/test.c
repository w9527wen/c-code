#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//struct exa
//{
//	char a;
//	int b;
//	struct exa *s2;
//}s1;

//
//struct
//{
//	char a;
//	int b;
//}*ps;


//typedef struct
//{
//	char f;
//	int  i;
//
//}node; //    typedef 重命名结构体 为 node 



//typedef struct
//{
//	char f;
//	int  i;
//	struct node* exa;
//}node;



//struct score
//{
//	int a;
//	int b;
//}s1;  //  声明结构体，同时创建变量 s1 
//
//struct score
//{
//	int a;
//	int b;
//}s2 = { 2,4 };  //  声明结构体，同时创建变量 s2 并初始化
//
//struct score s3;   //  定义结构体变量 s3 
//
//struct score s4 = { 1,2 }; // 定义结构体变量s4 并初始化
//
//
//struct score1
//{
//	int a;
//	int n;
//	struct score* node;
//}n1 = {2,4,NULL};  //自引用结构体初始化
//
//
//struct score1 n2  = {2,5,NULL}   //   定义n2  并初始化


//int main()
//{ 
//	// struct node s1 = { 'd' ,2 }
//	//node s1 = { 'd',2 };   //相对于普通的结构体，在创建变量时简化了一些。
//}




//enum score  //成绩
//{
//	A,
//	B,
//	C,
//	D,
//	E
//};
//
//
//enum score stu1 = A;




//enum sex //  性别
//{
//	male = 1,
//	female = 0
//};




//union un  
//{
//	char c ;
//	int i;
//};
//
// int main()
//{
//	 union un s1;
//	 s1.i = 1;
//	 printf("%d", s1.c);
//}





//struct S1
//{
//	int i;
//	char c1;
//	char c2;
//};




struct S3
{
	double d;

	char c;

	int i;
};

struct S4
{
	char c1;
	struct S3 s3;
	double d;

};

int main()
{
	printf("%d", sizeof(struct S4));
}