#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//
//int main()
//{
//
//	int a = 3;
//
//	int b = 4;
//
//	a = a ^ b;
//
//	b = a ^ b;
//
//	a = a ^ b;
//
//	printf("%d %d", a, b);
//	return 0;
//}

//#include<stdio.h>
//
//int main()
//{
//	int i = 0, a = 0, b = 2, c = 3, d = 4;
//	//i = a++ && ++b && d++;
//	i = a++ || ++b || d++;
//	
//	printf("%d %d %d %d\n", a, b, c, d);
//}

//#include<stdio.h>
//
//int test(int arr[], int sz)
//{
//	int i = 0;
//	int ret = 0;
//	for (i = 0; i < sz; i++)
//	{
//		ret ^= arr[i];
//	}
//	return ret;
//}
//
//int main()
//{
//	int arr[9] = { 1,2,3,4,5,1,2,3,4 };
//	int sz = (sizeof(arr) / sizeof(arr[0]));
//	printf("%d",test(arr, sz));
//}

#include<stdio.h>
int test(int arr[], int sz)
{
	int single1 = 0;
	int single2 = 0;
	int i = 0;
	int ret = 0;
	for (i = 0; i < sz; i++)
	{
		ret ^= arr[i];    //将所有数据异或，得到两个不同的数的异或的结果。
	}
	int pos = 0;
	for (i = 0; i < 32; i++)
	{
		if (((ret >> i) & 1) == 1)      //		相异为1 ，找到两个数不同的那个二进制位并记录
		{
			pos = i;
			break;         
		}
	}
	for (i = 0; i < sz; i++)
	{
		if (((arr[i] >> pos)&1) == 1)
		{
			single1 ^= arr[i];   //  将数据分为俩组，不同的那个比特位为1的，分为一组，并异或，得到一个单身的数据
		}
	}
	single2 = ret ^ single1;
	printf("%d %d", single1, single2);
}
int main()
{
	int arr[] = { 1,2,3,4,5,1,2,3,4,6 };
	int sz = (sizeof(arr) / sizeof(arr[0]));
	test(arr, sz);
	return 0;
}